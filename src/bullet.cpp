#include "bullet.h"

Bullet::Bullet( Game* game )
{
    this->game = game;

    this->game->gfx->Load( "data/bullet.png", "bullet" );
}

void Bullet::Add( float dx, float dy )
{
    float x = 280;
    float y = (480 / 2) - 16;
    float dir = atan2( dy - y, dx - x );

    BulletData bd;
    bd.x = x;
    bd.y = y;
    bd.velX = 5 * cos( dir );
    bd.velY = 5 * sin( dir );

    this->bullets.push_back( bd );
}

void Bullet::Update()
{
    for ( unsigned int i=0; i<this->bullets.size(); i++ )
    {
        this->bullets[i].x += this->bullets[i].velX;
        this->bullets[i].y += this->bullets[i].velY;
    }
}

void Bullet::Render()
{
    for ( unsigned int i=0; i<this->bullets.size(); i++ )
        this->game->gfx->Blit( "bullet", this->bullets[i].x, this->bullets[i].y );
}
