#include "level.h"

Level::Level( Game* game )
{
    this->game = game;

    this->game->gfx->Load( "data/light.png", "light" );
    this->game->gfx->Load( "data/grass.png", "grass" );

    this->offsetX = 0;
    this->offsetY = 0;
}

void Level::Move( float x, float y )
{
    this->offsetX += x;
    this->offsetY += y;

    if ( this->offsetX > 32 || this->offsetX < -32 ) this->offsetX = 0;
    if ( this->offsetY > 32 || this->offsetY < -32 ) this->offsetY = 0;
}

void Level::Update()
{

}

ALLEGRO_COLOR premul_alpha(ALLEGRO_COLOR color) {
    float r, g, b, a;
    al_unmap_rgba_f(color, &r, &g, &b, &a);
    return al_map_rgba_f(r * a, g * a, b * a, a);
}

void Level::Render()
{
    for ( int y=-1; y<16; y++ )
    {
        for ( int x=-1; x<21; x++ )
        {
            this->game->gfx->Blit( "grass", (x * 32) + this->offsetX, (y * 32) + this->offsetY );
        }
    }
}
