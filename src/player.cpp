#include "player.h"

Player::Player( Game* game )
{
    this->game = game;

    al_set_new_bitmap_flags( ALLEGRO_MAG_LINEAR | ALLEGRO_MIN_LINEAR );

    this->light = al_create_bitmap( 640, 480 );

    this->game->gfx->Load( "data/player_strip.png", "player" );
    this->game->gfx->Load( "data/flashlight.png", "flashlight" );
    this->game->gfx->Load( "data/gun_flash.png", "gun_flash" );
    this->game->gfx->Load( "data/gui.png", "gui" );

    for ( int i=0; i<3; i++ )
    {
        this->playerFrames[i] = al_create_bitmap( 64, 64 );
        al_set_target_bitmap( this->playerFrames[i] );
        al_draw_bitmap_region( this->game->gfx->GetBitmap( "player" ), i * 64, 0, 64, 64, 0, 0, 0 );
    }

    this->game->gfx->Load( "data/crosshair.png", "crosshair" );
    this->game->gfx->Load( "data/heart.png", "heart" );

    this->isMoving = false;
    this->actionKeyDown = false;
    this->flashlightOn = true;
    this->shootTimer = 0;
}

Player::~Player()
{
    for ( int i=0; i<3; i++ )
        al_destroy_bitmap( this->playerFrames[i] );

    al_destroy_bitmap( this->light );
}

void Player::Animate()
{
    if ( this->isMoving )
    {
        this->frameTimer++;

        if ( this->frameTimer == 40 )
            this->frameTimer = 0;

        if ( this->frameTimer == 0 )
            this->frame = 0;

        if ( this->frameTimer == 10 )
            this->frame = 1;

        if ( this->frameTimer == 20 )
            this->frame = 0;

        if ( this->frameTimer == 30 )
            this->frame = 2;
    }
    else
    {
        this->frameTimer = 0;
        this->frame = 0;
    }
}

void Player::Shoot()
{
    //int mx = this->game->input->GetMouseAxis().x;
    //int my = this->game->input->GetMouseAxis().y;

    //this->game->bullet->Add( mx, my );

    this->shootTimer = 20;
}

void Player::Update()
{
    if ( this->game->input->KeyDown( ALLEGRO_KEY_W ) )
    {
        this->game->level->Move( 0, 3 );
        this->isMoving = true;
    }

    if ( this->game->input->KeyDown( ALLEGRO_KEY_S ) )
    {
        this->game->level->Move( 0, -3 );
        this->isMoving = true;
    }

    if ( this->game->input->KeyDown( ALLEGRO_KEY_A ) )
    {
        this->game->level->Move( 3, 0 );
        this->isMoving = true;
    }

    if ( this->game->input->KeyDown( ALLEGRO_KEY_D ) )
    {
        this->game->level->Move( -3, 0 );
        this->isMoving = true;
    }

    if ( this->game->input->KeyUp( ALLEGRO_KEY_W ) && this->game->input->KeyUp( ALLEGRO_KEY_S ) && this->game->input->KeyUp( ALLEGRO_KEY_A ) && this->game->input->KeyUp( ALLEGRO_KEY_D ) )
        this->isMoving = false;

    if ( this->game->input->MouseDown( 1 ) && this->shootTimer == 0 )
        this->Shoot();
    else if ( this->game->input->MouseUp( 1 ) )
        this->shootTimer = 0;

    if ( this->shootTimer > 0 )
        this->shootTimer--;

    this->Animate();

    if ( this->game->input->KeyDown( ALLEGRO_KEY_F ) && !this->actionKeyDown )
    {
        this->flashlightOn = !this->flashlightOn;
        this->actionKeyDown = true;
    }
    else if ( this->game->input->KeyUp( ALLEGRO_KEY_F ) )
        this->actionKeyDown = false;
}

void Player::Render()
{
    int x = (640 / 2) - ((this->game->gfx->Width( "player" ) / 3) / 2);
    int y = (480 / 2) - ((this->game->gfx->Height( "player" ) / 3) / 2);
    int mx = this->game->input->GetMouseAxis().x;
    int my = this->game->input->GetMouseAxis().y;

    float angle = atan2( my - y, mx - x );

    if ( this->flashlightOn )
    {
        al_set_target_bitmap( this->light );
        al_clear_to_color( al_map_rgba( 0, 0, 0, 190 ) );
        int a, b, c;
        al_get_blender( &a, &b, &c );

        al_set_blender( ALLEGRO_DEST_MINUS_SRC , ALLEGRO_ONE , ALLEGRO_ONE );
        this->game->gfx->BlitRotated( "flashlight", 64, 64, x, y, angle );
        al_set_blender( a, b, c );
        al_set_target_bitmap( al_get_backbuffer( this->game->display->GetDisplay() ) );
    }
    else
    {
        al_set_target_bitmap( this->light );
        al_clear_to_color( al_map_rgba( 0, 0, 0, 190 ) );
        al_set_target_bitmap( al_get_backbuffer( this->game->display->GetDisplay() ) );
    }

    this->game->gfx->Blit( "player", 50, 140 );

    al_draw_bitmap( this->light, 0, 0, 0 );

    al_draw_rotated_bitmap( this->playerFrames[this->frame], 32, 32, x, y, angle, 0 );

    this->game->gfx->Blit( "heart", 630 - 106, 10 );
    this->game->gfx->Blit( "heart", 630 - 74, 10 );
    this->game->gfx->Blit( "heart", 630 - 42, 10 );

    //this->game->gfx->Blit( "gui", 0, 0 );

    if ( this->shootTimer > 15 )
        this->game->gfx->BlitRotated( "gun_flash", 32, 32, x, y, angle );

    this->game->gfx->Blit( "crosshair", mx - (this->game->gfx->Height( "crosshair" ) / 2), my - (this->game->gfx->Width( "crosshair" ) / 2) );
}
