#include "game.h"

int main( int argc, char** argv )
{
    Game* game = new Game();

    game->Init();
    game->Run();
    game->CleanUp();

    delete game;

    return 0;
}
