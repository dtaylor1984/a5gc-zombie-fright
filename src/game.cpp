#include "game.h"

void Game::Init()
{
    this->eventManager = new EventManager();
    this->display = new Display( 640, 480, ALLEGRO_FULLSCREEN );
    this->gameTick = new Timer( 1.0 / 60 );
    this->fpsTimer = new Timer( 1 );
    this->input = new Input( eventManager );
    this->gfx = new Graphics();
    this->font = new Font();
    //this->sound = new Sound();

    this->eventManager->AddSource( this->display->GetEventSource() );
    this->eventManager->AddSource( this->gameTick->GetEventSource() );
    this->eventManager->AddSource( this->fpsTimer->GetEventSource() );

    this->player = new Player( this );
    this->level = new Level( this );
    this->bullet = new Bullet( this );

    this->display->SetTitle( "Zombie Fright" );

    this->gameTick->Start();
    this->fpsTimer->Start();

    this->input->HideMouse( this->display->GetDisplay() );

    this->font->Load( "data/micross.ttf", "mainfont", 13 );

    this->isRunning = true;
    this->state = GAME_STATE_PLAY;

    this->fps = 0;
    this->frameCounter = 0;
}

void Game::Run()
{
    while ( this->isRunning )
    {
        ALLEGRO_EVENT event = this->eventManager->GetNextEvent();

        input->Update( event );

        switch ( event.type )
        {
            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                this->isRunning = false;
            break;

            case ALLEGRO_EVENT_TIMER:
                if ( event.timer.source == this->gameTick->GetTimer() )
                    this->Update();

                if ( event.timer.source == this->fpsTimer->GetTimer() )
                {
                    this->fps = this->frameCounter;
                    this->frameCounter = 0;
                }
            break;
        }

        if ( this->input->KeyDown( ALLEGRO_KEY_ESCAPE ) )
            this->isRunning = false;

        if ( this->input->KeyDown( ALLEGRO_KEY_F12 ) )
        {
            ALLEGRO_BITMAP* screen = al_get_backbuffer( this->display->GetDisplay() );
            al_save_bitmap( "screenshot.png", screen );
            al_destroy_bitmap( screen );
        }

        this->Render();
    }
}

void Game::CleanUp()
{
    delete this->eventManager;
    delete this->display;
    delete this->gameTick;
    delete this->fpsTimer;
    delete this->input;
    delete this->gfx;
    delete this->font;
    delete this->sound;

    delete this->player;
    delete this->level;
    delete this->bullet;
}

void Game::Update()
{
    switch ( this->state )
    {
        case GAME_STATE_MENU:
        break;

        case GAME_STATE_PLAY:
            this->level->Update();
            this->player->Update();
            this->bullet->Update();
        break;
    }
}

void Game::Render()
{
    this->display->Clear();

    switch ( this->state )
    {
        case GAME_STATE_MENU:
        break;

        case GAME_STATE_PLAY:
            this->level->Render();
            this->player->Render();
            this->bullet->Render();
        break;
    }

    char buffer[10];
    sprintf( buffer, "FPS: %i", this->fps );

    if ( this->input->KeyDown( ALLEGRO_KEY_F1 ) )
        this->font->Blit( "mainfont", buffer, 10, 10, al_map_rgb( 255, 255, 255 ) );

    this->display->Flip();

    this->frameCounter++;
}
