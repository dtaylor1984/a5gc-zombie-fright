#ifndef __COMMON_H_INCLUDED
#define __COMMON_H_INCLUDED

#include <a5gc.h>

#include <cstdio>
#include <math.h>

#include "fwd/game.h"
#include "fwd/player.h"
#include "fwd/bullet.h"
#include "fwd/level.h"

#include "game.h"
#include "player.h"
#include "bullet.h"
#include "level.h"

#endif // __COMMON_H_INCLUDED
