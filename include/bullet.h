#ifndef __BULLET_H_INCLUDED
#define __BULLET_H_INCLUDED

#include "common.h"

class Bullet
{
    private:
        Game* game;

        std::vector<BulletData> bullets;

    public:
        Bullet( Game* game );

        void Add( float dx, float dy );
        void Update();
        void Render();
};

#endif // __BULLET_H_INCLUDED
