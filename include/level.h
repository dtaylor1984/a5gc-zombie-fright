#ifndef __LEVEL_H_INCLUDED
#define __LEVEL_H_INCLUDED

#include "common.h"

class Level
{
    private:
        Game* game;

        float offsetX, offsetY;

    public:
        Level( Game* game );

        void Move( float x, float y );
        void Update();
        void Render();
};

#endif // __LEVEL_H_INCLUDED
