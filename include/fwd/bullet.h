#ifndef __FWD_BULLET_H_INCLUDED
#define __FWD_BULLET_H_INCLUDED

struct BulletData
{
    float x;
    float y;
    float velX;
    float velY;
};

class Bullet;

#endif // __FWD_BULLET_H_INCLUDED
