#ifndef __FWD_GAME_H_INCLUDED
#define __FWD_GAME_H_INCLUDED

enum GAME_STATE
{
    GAME_STATE_MENU,
    GAME_STATE_PLAY,
};

class Game;

#endif // __FWD_GAME_H_INCLUDED
