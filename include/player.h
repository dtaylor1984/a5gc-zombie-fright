#ifndef __PLAYER_H_INCLUDED
#define __PLAYER_H_INCLUDED

#include "common.h"

class Player
{
    private:
        Game* game;

        ALLEGRO_BITMAP* playerFrames[3];
        ALLEGRO_BITMAP* light;
        bool isMoving;
        int frame;
        int frameTimer;

        bool actionKeyDown;
        bool flashlightOn;

        int shootTimer;

        void Animate();
        void Shoot();

    public:
        Player( Game* game );
        ~Player();

        void Update();
        void Render();
};

#endif // __PLAYER_H_INCLUDED
