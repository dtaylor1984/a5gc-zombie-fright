#ifndef __GAME_H_INCLUDED
#define __GAME_H_INCLUDED

#include "common.h"

class Game
{
    private:
        EventManager* eventManager;
        Timer* gameTick;
        Timer* fpsTimer;

        bool isRunning;
        GAME_STATE state;
        int fps;
        int frameCounter;

        void Update();
        void Render();

    public:
        Display* display;
        Input* input;
        Graphics* gfx;
        Font* font;
        Sound* sound;

        Player* player;
        Level* level;
        Bullet* bullet;

        void Init();
        void Run();
        void CleanUp();
};

#endif // __GAME_H_INCLUDED
